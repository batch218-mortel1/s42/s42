const express = require("express");
const router = express.Router(); 
const courseController = require("../controllers/productController.js");
const auth = require("../auth.js");

router.post("/create", (req, res) => {
	productController.addProduct(req.body).then(resultFromController => res.send(resultFromController))
})

router.get("/all", (req, res) => {
	productController.getAllProduct().then(resultFromController => res.send(resultFromController))
})

router.get("/active", (req, res) => {
	productController.getActiveProducts().then(resultFromController => res.send(resultFromController))
})

router.get("/:productId", (req, res) => {
	productController.getProduct(req.params.productId).then(resultFromController => res.send(resultFromController))
})

router.patch("/:productId/update", auth.verify, (req,res) => 
{
	const newData = {
		productName: req.body, 	
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.updateProduct(req.params.productId, newData).then(resultFromController => {
		res.send(resultFromController)
	})
})

  


module.exports = router;

